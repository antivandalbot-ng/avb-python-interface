from revscoring import Datasource, Feature
from revscoring.features import wikitext
from deltas import wikitext_split
from collections import Counter
import itertools
import json
import nltk
import pickle
import spacy
from nltk.corpus import names, stopwords


with open("config.json") as c:
    config = json.load(c)
nlp = spacy.load("en_core_web_sm", disable=["parser", "ner"])


def _process_tfidf(doc):
    try:
        Tfidf_vect = pickle.load(open(config['tfidfmodel'], "rb"))
    except FileNotFoundError:
        return 0
    tokens = ["{}_{}".format(token.text.lower(), token.tag_) for token in doc if not token.is_stop]
    return Tfidf_vect.transform([' '.join(tokens)])


def _process_nb(tfidf):
    try:
        nb = pickle.load(open(config['naivebayesmodel'], "rb"))
    except FileNotFoundError:
        return 0.0
    if type(tfidf) is int:
        return 0.0
    # Returns vandalism probability
    return nb.predict_proba(tfidf)[0][1]


def _process_female_names(words):
    return len(set(words) & set(names.words('female.txt')))


def _process_male_names(words):
    return len(set(words) & set(names.words('male.txt')))


def _process_tokens(words):
    doc = nlp(' '.join(words))
    return doc


def _process_counter(tokens):
    return Counter([x.pos_ for x in tokens])


def _count_nouns(counter):
    return counter['NOUN'] + counter['PROPN']


def _count_verbs(counter):
    return counter['VERB']


def _count_pronouns(counter):
    return counter['PRON']


def _count_adjectives(counter):
    return counter['ADJ']


def _count_adverbs(counter):
    return counter['ADV']


def _count_adpositions(counter):
    return counter['ADP']


def _count_conjunctions(counter):
    return counter['CONJ'] + counter['CCONJ'] + counter['SCONJ']


def _count_determiners(counter):
    return counter['DET']


def _count_particles(counter):
    return counter['PART']


def _count_other(counter):
    return counter['X']


words_added_tagged = Datasource(
    "words_added_tagged", _process_tokens,
    depends_on=[wikitext.revision.diff.datasources.words_added]
)
words_added_tfidf = Datasource(
    "words_added_tfidf", _process_tfidf,
    depends_on=[words_added_tagged]
)
nb_score = Feature(
    "nb_score", _process_nb,
    depends_on=[words_added_tfidf],
    returns=float
)
female_names_added = Feature(
    "female_names_added", _process_female_names,
    depends_on=[wikitext.revision.diff.datasources.words_added],
    returns=int
)
male_names_added = Feature(
    "male_names_added", _process_male_names,
    depends_on=[wikitext.revision.diff.datasources.words_added],
    returns=int
)
pos_counter = Datasource(
    "pos_counter", _process_counter,
    depends_on=[words_added_tagged]
)
nouns_added = Feature(
    "nouns_added", _count_nouns,
    depends_on=[pos_counter],
    returns=int
)
verbs_added = Feature(
    "verbs_added", _count_verbs,
    depends_on=[pos_counter],
    returns=int
)
pronouns_added = Feature(
    "pronouns_added", _count_pronouns,
    depends_on=[pos_counter],
    returns=int
)
adjectives_added = Feature(
    "adjectives_added", _count_adjectives,
    depends_on=[pos_counter],
    returns=int
)
adverbs_added = Feature(
    "adverbs_added", _count_adverbs,
    depends_on=[pos_counter],
    returns=int
)
adpositions_added = Feature(
    "adpositions_added", _count_adpositions,
    depends_on=[pos_counter],
    returns=int
)
conjunctions_added = Feature(
    "conjunctions_added", _count_conjunctions,
    depends_on=[pos_counter],
    returns=int
)
determiners_added = Feature(
    "determiners_added", _count_determiners,
    depends_on=[pos_counter],
    returns=int
)
particles_added = Feature(
    "particles_added", _count_particles,
    depends_on=[pos_counter],
    returns=int
)
other_added = Feature(
    "other_added", _count_other,
    depends_on=[pos_counter],
    returns=int
)