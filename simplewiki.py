from revscoring import datasources
from revscoring.features.meta import aggregators
from revscoring.features import bytes, temporal, wikitext
from revscoring.features.modifiers import log, max
from revscoring.languages.english import badwords, dictionary, informals, stopwords
import custom_features

proportion_of_badwords_added = badwords.revision.diff.matches_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_badwords_removed = badwords.revision.diff.matches_removed / max(wikitext.revision.diff.words_removed, 1)
proportion_of_informals_added = informals.revision.diff.matches_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_informals_removed = informals.revision.diff.matches_removed / max(wikitext.revision.diff.words_removed, 1)
proportion_of_misspellings_added = dictionary.revision.diff.non_dict_words_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_misspellings_removed = dictionary.revision.diff.non_dict_words_removed / max(wikitext.revision.diff.words_removed, 1)
proportion_of_stopwords_added = stopwords.revision.diff.stopwords_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_stopwords_removed = stopwords.revision.diff.stopwords_removed / max(wikitext.revision.diff.words_removed, 1)

proportion_of_badwords = badwords.revision.parent.matches / max(wikitext.revision.parent.words, 1)
proportion_of_informals = informals.revision.parent.matches / max(wikitext.revision.parent.words, 1)
proportion_of_misspellings = dictionary.revision.parent.non_dict_words / max(wikitext.revision.parent.words, 1)
proportion_of_stopwords = stopwords.revision.parent.stopwords / max(wikitext.revision.parent.words, 1)

added_badwords_ratio = proportion_of_badwords_added / max(proportion_of_badwords, 0.01)
added_informals_ratio = proportion_of_informals_added / max(proportion_of_informals, 0.01)
added_misspellings_ratio = proportion_of_misspellings_added / max(proportion_of_misspellings, 0.01)
added_stopwords_ratio = proportion_of_stopwords_added / max(proportion_of_stopwords, 0.01)

bytes_changed = bytes.revision.length - bytes.revision.parent.length
bytes_changed_ratio = bytes_changed / max(bytes.revision.parent.length, 1)

proportion_of_nouns_added = custom_features.nouns_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_verbs_added = custom_features.verbs_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_pronouns_added = custom_features.pronouns_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_adjectives_added = custom_features.adjectives_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_adverbs_added = custom_features.adverbs_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_adpositions_added = custom_features.adpositions_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_conjunctions_added = custom_features.conjunctions_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_determiners_added = custom_features.determiners_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_particles_added = custom_features.particles_added / max(wikitext.revision.diff.words_added, 1)
proportion_of_other_added = custom_features.other_added / max(wikitext.revision.diff.words_added, 1)

damaging = [
    log(wikitext.revision.diff.chars_added + 1),
    log(wikitext.revision.diff.chars_removed + 1),
    log(wikitext.revision.diff.numeric_chars_added + 1),
    log(wikitext.revision.diff.numeric_chars_removed + 1),
    log(wikitext.revision.diff.whitespace_chars_added + 1),
    log(wikitext.revision.diff.whitespace_chars_removed + 1),
    log(wikitext.revision.diff.markup_chars_added + 1),
    log(wikitext.revision.diff.markup_chars_removed + 1),
    log(wikitext.revision.diff.entity_chars_added + 1),
    log(wikitext.revision.diff.entity_chars_removed + 1),
    log(wikitext.revision.diff.word_chars_added + 1),
    log(wikitext.revision.diff.word_chars_removed + 1),
    log(wikitext.revision.diff.uppercase_word_chars_added + 1),
    log(wikitext.revision.diff.uppercase_word_chars_removed + 1),
    log(wikitext.revision.diff.punctuation_chars_added + 1),
    log(wikitext.revision.diff.punctuation_chars_removed + 1),
    log(wikitext.revision.diff.break_chars_added + 1),
    log(wikitext.revision.diff.break_chars_removed + 1),
    wikitext.revision.diff.token_delta_increase,
    wikitext.revision.diff.token_delta_decrease,
    wikitext.revision.diff.token_prop_delta_increase,
    wikitext.revision.diff.token_prop_delta_decrease,
    wikitext.revision.diff.number_delta_increase,
    wikitext.revision.diff.number_delta_decrease,
    wikitext.revision.diff.number_prop_delta_increase,
    wikitext.revision.diff.number_prop_delta_decrease,
    wikitext.revision.diff.whitespace_delta_increase,
    wikitext.revision.diff.whitespace_delta_decrease,
    wikitext.revision.diff.whitespace_prop_delta_increase,
    wikitext.revision.diff.whitespace_prop_delta_decrease,
    wikitext.revision.diff.markup_delta_increase,
    wikitext.revision.diff.markup_delta_decrease,
    wikitext.revision.diff.markup_prop_delta_increase,
    wikitext.revision.diff.markup_prop_delta_decrease,
    wikitext.revision.diff.entity_delta_increase,
    wikitext.revision.diff.entity_delta_decrease,
    wikitext.revision.diff.entity_prop_delta_increase,
    wikitext.revision.diff.entity_prop_delta_decrease,
    wikitext.revision.diff.word_delta_increase,
    wikitext.revision.diff.word_delta_decrease,
    wikitext.revision.diff.word_prop_delta_increase,
    wikitext.revision.diff.word_prop_delta_decrease,
    wikitext.revision.diff.uppercase_word_delta_increase,
    wikitext.revision.diff.uppercase_word_delta_decrease,
    wikitext.revision.diff.uppercase_word_prop_delta_increase,
    wikitext.revision.diff.uppercase_word_prop_delta_decrease,
    wikitext.revision.diff.punctuation_delta_increase,
    wikitext.revision.diff.punctuation_delta_decrease,
    wikitext.revision.diff.punctuation_prop_delta_increase,
    wikitext.revision.diff.punctuation_prop_delta_decrease,
    wikitext.revision.diff.break_delta_increase,
    wikitext.revision.diff.break_delta_decrease,
    wikitext.revision.diff.break_prop_delta_increase,
    wikitext.revision.diff.break_prop_delta_decrease,
    log(wikitext.revision.diff.segments_added + 1),
    log(wikitext.revision.diff.segments_removed + 1),
    log(wikitext.revision.diff.tokens_added + 1),
    log(wikitext.revision.diff.tokens_removed + 1),
    log(wikitext.revision.diff.numbers_added + 1),
    log(wikitext.revision.diff.numbers_removed + 1),
    log(wikitext.revision.diff.whitespaces_added + 1),
    log(wikitext.revision.diff.whitespaces_removed + 1),
    log(wikitext.revision.diff.markups_added + 1),
    log(wikitext.revision.diff.markups_removed + 1),
    log(wikitext.revision.diff.entities_added + 1),
    log(wikitext.revision.diff.entities_removed + 1),
    log(wikitext.revision.diff.words_added + 1),
    log(wikitext.revision.diff.words_removed + 1),
    log(wikitext.revision.diff.uppercase_words_added + 1),
    log(wikitext.revision.diff.uppercase_words_removed + 1),
    log(wikitext.revision.diff.punctuations_added + 1),
    log(wikitext.revision.diff.punctuations_removed + 1),
    log(wikitext.revision.diff.breaks_added + 1),
    log(wikitext.revision.diff.breaks_removed + 1),
    wikitext.revision.diff.longest_token_added,
    wikitext.revision.diff.longest_repeated_char_added,
    bytes_changed,
    bytes_changed_ratio,
    wikitext.revision.parent.longest_repeated_char,
    log(wikitext.revision.parent.words + 1),
    log(wikitext.revision.words + 1),
    log(wikitext.revision.parent.uppercase_words + 1),
    log(wikitext.revision.uppercase_words + 1),
    log(wikitext.revision.parent.numbers + 1),
    log(wikitext.revision.numbers + 1),
    log(wikitext.revision.parent.punctuations + 1),
    log(wikitext.revision.punctuations + 1),
    log(temporal.revision.user.seconds_since_registration + 1),
    added_badwords_ratio,
    added_informals_ratio,
    added_misspellings_ratio,
    added_stopwords_ratio,
    log(badwords.revision.diff.matches_added + 1),
    log(badwords.revision.diff.matches_removed + 1),
    log(informals.revision.diff.matches_added + 1),
    log(informals.revision.diff.matches_removed + 1),
    log(dictionary.revision.diff.non_dict_words_added + 1),
    log(dictionary.revision.diff.non_dict_words_removed + 1),
    log(stopwords.revision.diff.stopwords_added + 1),
    log(stopwords.revision.diff.stopwords_removed + 1),
    proportion_of_badwords_added,
    proportion_of_badwords_removed,
    proportion_of_informals_added,
    proportion_of_informals_removed,
    proportion_of_misspellings_added,
    proportion_of_misspellings_removed,
    proportion_of_stopwords_added,
    proportion_of_stopwords_removed,
    proportion_of_nouns_added,
    proportion_of_verbs_added,
    proportion_of_pronouns_added,
    proportion_of_adjectives_added,
    proportion_of_adverbs_added,
    proportion_of_adpositions_added,
    proportion_of_conjunctions_added,
    proportion_of_determiners_added,
    proportion_of_particles_added,
    proportion_of_other_added,
    wikitext.revision.headings - wikitext.revision.parent.headings,
    wikitext.revision.wikilinks - wikitext.revision.parent.wikilinks,
    wikitext.revision.external_links - wikitext.revision.parent.external_links,
    wikitext.revision.ref_tags - wikitext.revision.parent.ref_tags,
    wikitext.revision.templates - wikitext.revision.parent.templates,
    wikitext.revision.content_chars - wikitext.revision.parent.content_chars,
    log(aggregators.len(datasources.revision_oriented.revision.comment) + 1),
    custom_features.nb_score,
    custom_features.female_names_added,
    custom_features.male_names_added
]
