import ipaddress
import json
import re
from gettext import ngettext

from dateutil import relativedelta

with open("config.json") as c:
    config = json.load(c)

prog1 = re.compile(r"<!--\s?Template:(?P<type>uw-[a-z]*(?P<level>\d)(im)?|Blatantvandal \(serious warning\)|RepeatVandal)\s?-->.*?User:(?P<warninguser>.*?)(?:\||\]\]).*?(?P<hour>\d{2}):(?P<minute>\d{2}), (?P<day>\d+) (?P<month>[a-zA-Z]+) (?P<year>\d{4}) \(UTC\)",
                   re.IGNORECASE)
prog2 = re.compile(r"User:(?P<warninguser>.*?)(?:\||\]\]).*?(?P<hour>\d{2}):(?P<minute>\d{2}), (?P<day>\d+) (?P<month>[a-zA-Z]+) (?P<year>\d{4}) \(UTC\)<!-- Template:(?P<type>uw-huggle(?P<level>\d)(im)?) -->",
                   re.IGNORECASE)

overall_aiv_pattern = re.compile(r"(\* *{{{{(?:ip)?vandal\|(?:.+?)}}}}.*User:(?:.+?)(?:\||\]\]).*?(?:\d{{2}}):(?:\d{{2}}), (?:\d+) (?:[a-zA-Z]+) (?:\d{{4}}) \(UTC\)(?:(?:\s+?.+User:(?:(?!{}).*?)(?:\||\]\]).*?(?:\d{{2}}):(?:\d{{2}}), (?:\d+) (?:[a-zA-Z]+) (?:\d{{4}}) \(UTC\))*))".format(config['wikiuser']),
                         re.IGNORECASE)
aiv_pattern = re.compile(r"\* *{{{{(?:ip)?vandal\|(?P<user>.+?)}}}}.*User:(?P<reporter>.+?)(?:\||\]\]).*?(?P<hour>\d{{2}}):(?P<minute>\d{{2}}), (?P<day>\d+) (?P<month>[a-zA-Z]+) (?P<year>\d{{4}}) \(UTC\)(?P<comments>(?:\s+?.+User:(?:(?!{}).*?)(?:\||\]\]).*?(?:\d{{2}}):(?:\d{{2}}), (?:\d+) (?:[a-zA-Z]+) (?:\d{{4}}) \(UTC\))*)".format(config['wikiuser']),
                         re.IGNORECASE)


def get_duration(expiry, timestamp):
    delta = relativedelta.relativedelta(expiry, timestamp)

    result = []
    if delta.years:
        result.append(ngettext("%d year", "%d years", delta.years) % delta.years)
    if delta.months:
        result.append(ngettext("%d month", "%d months", delta.months) % delta.months)
    if delta.days:
        result.append(ngettext("%d day", "%d days", delta.days) % delta.days)
    if delta.hours:
        result.append(ngettext("%d hour", "%d hours", delta.hours) % delta.hours)
    if delta.minutes:
        result.append(ngettext("%d minute", "%d minutes", delta.minutes) % delta.minutes)
    if delta.seconds:
        result.append(ngettext("%d second", "%d seconds", delta.seconds) % delta.seconds)

    overrides = {
        '1 day': '24 hours',
        '1 day, 7 hours': '31 hours',
        '2 days': '48 hours',
        '3 days': '72 hours',
        '4 days, 3 hours': '99 hours',
        '4 days, 4 hours': '100 hours',
        '7 days': '1 week',
        '14 days': '2 weeks',
    }

    result = ', '.join(result)
    try:
        return overrides[result]
    except KeyError:
        return result


def is_ipaddress(value):
    try:
        ipaddress.ip_address(value)
    except ValueError:
        return False
    else:
        return True
