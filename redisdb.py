import asyncio
import logging
from datetime import datetime, timezone

import aioredis


class RedisDB:
    def __init__(self, url="redis://localhost", db=0):
        self.logger = logging.getLogger("antivandalbot.{}".format(__name__))
        self._pool = None
        asyncio.ensure_future(self._setup(url, db))

    async def _setup(self, url, db):
        self._pool = await aioredis.create_redis_pool(url, db=db, encoding="utf-8", maxsize=5)

    async def close(self):
        self._pool.close()
        await self._pool.wait_closed()

    async def get_last_revert_time(self, user, title):
        revert_time = await self._pool.get("vandal:{}:{}".format(user, title))
        if revert_time is not None:
            return datetime.fromtimestamp(float(revert_time), timezone.utc)
        else:
            return datetime.fromtimestamp(0, timezone.utc)

    async def set_last_revert_time(self, user, title, time=None):
        if time is None:
            time = datetime.now(timezone.utc).timestamp()
        await self._pool.setex("vandal:{}:{}".format(user, title), 24*60*60, time)
