import asyncio
import calendar
import copy
import ipaddress
import json
import logging
import re
import time
from datetime import datetime

from dateutil import parser

import utils


class BotCore:

    def __init__(self, api, ipc):
        self.logger = logging.getLogger("antivandalbot.{}".format(__name__))
        with open("config.json") as c:
            self.config = json.load(c)
        self.api = api
        self.ipc = ipc

    def reload(self):
        with open("config.json") as c:
            self.config = json.load(c)

    async def warn(self, user, title, probability, revid):
        self.logger.debug("Attempting to warn {}...".format(user))
        tptitle = "User talk:{}".format(user)
        warning_level = 1
        now_datetime = datetime.utcnow()
        usertalk = await self.api.get_page(tptitle)
        if not usertalk:
            new_data = "== {:%B %Y} ==\n\n{{{{subst:User:{}/Uw-vandalism{}|{}|{:.5f}|{}}}}} --~~~~".format(
                now_datetime, self.config['wikiuser'], warning_level, title, probability, revid
            )
            summary_replacement = {
                "warninglevel": warning_level,
                "title": title
            }
            summary = self.config['warnsummary'].format(**summary_replacement)
            await self.api.edit(tptitle, new_data, summary)
        else:
            tpcontent = usertalk['content']
            basetimestamp = usertalk['timestamp']
            try:
                matches = list(utils.prog1.finditer(tpcontent))[-1]
            except IndexError:
                try:
                    matches = list(utils.prog2.finditer(tpcontent))[-1]
                except IndexError:
                    matches = None
            if matches:
                if matches.group("type").lower() == "blatantvandal (serious warning)" or matches.group("type").lower() == "repeatvandal":
                    warning_level = 4
                lastwarn = datetime(
                    year=int(matches.group("year")),
                    month=list(calendar.month_name).index(matches.group("month")),
                    day=int(matches.group("day")),
                    hour=int(matches.group("hour")),
                    minute=int(matches.group("minute"))
                )
                td = now_datetime - lastwarn
                if matches.group("level"):
                    warning_level = int(matches.group("level"))
                if td.total_seconds() <= 24*60*60:
                    # Last warned <= 24 hours ago
                    self.logger.debug("User last warned within the past 24 hours.")
                    warning_level += 1
                elif td.total_seconds() <= 36*60*60:
                    self.logger.debug("User last warned within the past 36 hours.")
                    warning_level -= 1
                else:
                    # Restart from level 1
                    self.logger.debug("User last warned >36 hours ago.")
                    warning_level = 1
            if warning_level <= 0:
                warning_level = 1
            if warning_level > 4:
                return await self.aiv(user, title)
            else:
                if re.search(r"==(\s)*?{:%B %Y}(\s)*?==".format(now_datetime), tpcontent, re.IGNORECASE):
                    # Month header already exists
                    new_data = "{}\n\n{{{{subst:User:{}/Uw-vandalism{}|{}|{:.5f}|{}}}}} --~~~~".format(
                        tpcontent, self.config['wikiuser'], warning_level, title, probability, revid
                    )
                else:
                    new_data = "{}\n\n== {:%B %Y} ==\n\n{{{{subst:User:{}/Uw-vandalism{}|{}|{:.5f}|{}}}}} --~~~~".format(
                        tpcontent, now_datetime, self.config['wikiuser'], warning_level, title, probability, revid
                    )
                summary = "Adding level {} warning about possible vandalism on [[{}]] (BOT)".format(warning_level, title)
                await self.api.edit(tptitle, new_data, summary, base_timestamp=basetimestamp)

    async def aiv(self, user, title):
        oldaiv = await self.api.get_page(self.config['aivpage'])
        oldaiv_content = oldaiv['content']
        new_data = None
        try:
            ipaddress.ip_address(user)
            if "{{{{ipvandal|{}}}}}".format(user) in oldaiv_content:
                await self.ipc.write("PRIVMSG {} :!admin@simplewiki [[simple:User:{}]] is vandalising while listed on [[simple:Wikipedia:Vandalism in progress/Bot]]".format(
                    self.config['cvnchannel'], user
                ))
            else:
                new_data = "{}\n* {{{{ipvandal|{}}}}} has been vandalising [[:{}]] --~~~~".format(
                    oldaiv_content, user, title
                )
        except ValueError:
            if "{{{{vandal|{}}}}}".format(user) in oldaiv_content:
                await self.ipc.write("PRIVMSG {} :!admin@simplewiki [[simple:User:{}]] is vandalising while listed on [[simple:Wikipedia:Vandalism in progress/Bot]]".format(
                    self.config['cvnchannel'], user
                ))
            else:
                # Registered user
                new_data = "{}\n* {{{{vandal|{}}}}} has been vandalising [[:{}]] --~~~~".format(
                    oldaiv_content, user, title
                )
        if new_data:
            await self.ipc.write("PRIVMSG {} :!admin@simplewiki Reporting [[simple:User:{}]] to [[simple:Wikipedia:Vandalism in progress/Bot]]".format(
                self.config['cvnchannel'], user
            ))
            summary = "Reporting [[Special:Contributions/{}|{}]] (BOT)".format(user, user)
            await self.api.edit(self.config['aivpage'], new_data, summary, bot=False)

    async def check_blocks(self):
        while True:
            try:
                start = time.perf_counter()
                aiv_contents = await self.api.get_page(self.config['aivpage'])
                entries = utils.overall_aiv_pattern.split(aiv_contents['content'])
                new_entries = copy.copy(entries)
                for i, entry in enumerate(entries):
                    entry_details = utils.aiv_pattern.match(entry)
                    if not entry_details:
                        if re.fullmatch(r"\n+", entry):
                            new_entries[i] = ""
                        continue
                    elif not entry_details.group("comments"):
                        # No other user has commented on this entry, check if removal possible
                        block = await self.api.get_block(entry_details.group("user"))
                        if not block:
                            # User is not blocked
                            continue
                        else:
                            if block['expiry'] == "infinity":
                                block_duration = "indef"
                            else:
                                try:
                                    block_duration = utils.get_duration(parser.parse(block['expiry']), parser.parse(block['timestamp']))
                                except ValueError:
                                    block_duration = "unknown duration"
                        new_entries[i] = ""
                        flags = []
                        if "anononly" in block:
                            flags.append("AO")
                        if "nocreate" in block:
                            flags.append("ACB")
                        if "noemail" in block:
                            flags.append("EMD")
                        if not "allowusertalk" in block:
                            flags.append("TPD")
                        summary = "remove [[Special:Contributions/{user}|{user}]] (blocked {} by {} ({}))".format(
                            block_duration, block['by'], ", ".join(flags), user=block['user']
                        )
                        await self.api.edit(self.config['aivpage'], "\n\n".join(new_entries), summary=summary, minor=True, bot=False)
                        break  # Process one at a time for now
            except:
                self.logger.exception("Something went wrong:")
            finally:
                end = time.perf_counter()
                self.logger.debug("check_blocks took {:.4f} seconds.".format(end - start))
                await asyncio.sleep(4 * 60)
