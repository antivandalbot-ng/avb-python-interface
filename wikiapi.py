import copy
import json
import logging
import time

import oauth2 as oauth

import utils


class WikiAPI:
    def __init__(self, url, session):
        self.logger = logging.getLogger("antivandalbot.{}".format(__name__))
        with open("config.json") as c:
            config = json.load(c)
            self._oauth_consumer_key = config['consumer_key']
            self._oauth_consumer_secret = config['consumer_secret']
            self._oauth_access_token = config['access_token']
            self._oauth_access_secret = config['access_secret']
        self.session = session
        self.url = url
        self.edit_token = ""
        self.token_age = 0

    async def get(self, url, data):
        headers = {}
        headers['Authorization'] = self._sign_request(url, "GET", data)
        async with self.session.get(url, params=data, headers=headers) as r:
            return await r.json()

    async def post(self, url, data):
        headers = {}
        headers['Authorization'] = self._sign_request(url, "POST", data)
        async with self.session.post(url, data=data, headers=headers) as r:
            return await r.json()

    def _sign_request(self, url, method, data):
        o_data = copy.copy(data)
        consumer = oauth.Consumer(key=self._oauth_consumer_key, secret=self._oauth_consumer_secret)
        token = oauth.Token(key=self._oauth_access_token, secret=self._oauth_access_secret)

        o_data['oauth_version'] = "1.0"
        o_data['oauth_nonce'] = oauth.generate_nonce()
        o_data['oauth_timestamp'] = str(int(time.time()))
        o_data['oauth_token'] = token.key
        o_data['oauth_consumer_key'] = consumer.key

        req = oauth.Request(method=method, url=url, parameters=o_data)
        signature_method = oauth.SignatureMethod_HMAC_SHA1()
        req.sign_request(signature_method, consumer, token)
        return req.to_header()['Authorization']

    async def get_user_info(self, user):
        params = {
            "format": "json",
            "action": "query",
            "list": "users",
            "usprop": "editcount|groups",
            "ususers": user
        }
        data = await self.get(self.url, params)
        return data['query']['users'][0]

    async def get_block(self, user):
        params = {
            "format": "json",
            "action": "query",
            "list": "blocks",
            "bklimit": 1,
            "bkprop": "id|user|by|timestamp|expiry|reason|flags"
        }
        if utils.is_ipaddress(user):
            params['bkip'] = user
        else:
            params['bkusers'] = user
        data = await self.get(self.url, params)
        if not data['query']['blocks']:
            return None
        else:
            return data['query']['blocks'][0]

    async def get_page(self, title, revid=None):
        params = {
            "format": "json",
            "formatversion": 2,
            "action": "query",
            "prop": "revisions",
            "titles": title,
            "rvlimit": "1",
            "rvprop": "ids|content|timestamp",
        }
        if revid:
            params['rvstartid'] = revid
        data = await self.get(self.url, params)
        try:
            return data['query']['pages'][0]['revisions'][0]
        except KeyError:
            return None

    async def last_good_user(self, vandal, title):
        params = {
            "format": "json",
            "formatversion": 2,
            "action": "query",
            "prop": "revisions",
            "titles": title,
            "rvlimit": "20",
            "rvprop": "user|ids|timestamp"
        }
        data = await self.get(self.url, params)
        for rev in data['query']['pages'][0]['revisions']:
            if rev['user'] != vandal:
                return rev['user'], rev['timestamp']
        return vandal, None

    async def get_edit_token(self):
        params = {
            "format": "json",
            "action": "query",
            "meta": "tokens"
        }
        data = await self.get(self.url, params)
        self.token_age = time.time()
        self.edit_token = data['query']['tokens']['csrftoken']
        return self.edit_token

    async def edit(self, title, data, summary="", minor=False, bot=True, section=None, base_timestamp=None):
        start_time = int(time.time())
        if start_time - int(self.token_age) >= 6*60*60:
            await self.get_edit_token()
        params = {
            "format": "json",
            "action": "edit",
            "title": title,
            "text": data
        }
        if summary:
            params['summary'] = summary
        if minor:
            params['minor'] = "true"
        else:
            params['notminor'] = "true"
        if bot:
            params['bot'] = "true"
        if section:
            params['section'] = section
        if base_timestamp:
            params['basetimestamp'] = base_timestamp
        params['starttimestamp'] = start_time
        params['token'] = self.edit_token
        data = await self.post(self.url, params)
        return data['edit']

    async def rollback(self, title, user, summary=None, bot=False):
        params = {
            "format": "json",
            "action": "query",
            "meta": "tokens",
            "type": "rollback"
        }
        tquery = await self.get(self.url, params)
        rtoken = tquery['query']['tokens']['rollbacktoken']
        params = {
            "format": "json",
            "action": "rollback",
            "title": title,
            "user": user,
            "token": rtoken
        }
        if bot:
            params['markbot'] = "true"
        if summary:
            params['summary'] = summary
        data = await self.post(self.url, params)
        return data
