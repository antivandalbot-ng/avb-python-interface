import json
import logging
from datetime import datetime

from mwapi import Session
from revscoring import Model
from revscoring.extractors import api


class EditScorer:
    def __init__(self, url, user_agent="revscoring"):
        self.logger = logging.getLogger("antivandalbot.{}".format(__name__))
        self.session = Session(url, user_agent=user_agent)
        self.api_extractor = api.Extractor(self.session)
        self.model_filename = None
        self.model_type = None
        self.model = None
        self.load()

    def score(self, rev):
        values = self.api_extractor.extract(int(rev), self.model.features)
        return self.model.score(values)

    def load(self):
        with open("config.json") as c:
            config = json.load(c)
            self.model_filename = config['modelname']
        with open(self.model_filename) as f:
            self.model = Model.load(f)
            self.model_type = type(self.model.estimator).__name__
            self.logger.info("Scoring model loaded, type: {}, version {}, trained on {:%Y-%m-%d %H:%M:%S}".format(
                self.model_type, self.model.version, datetime.fromtimestamp(self.model.trained)))
