import json
import logging
import time

import aiomysql


class ControlDB:
    def __init__(self, loop):
        self.logger = logging.getLogger("antivandalbot.{}".format(__name__))
        self.db_pool = None
        self.loop = loop
        with open("config.json") as c:
            config = json.load(c)
            self.dbhost = config['dbhost']
            self.dbuser = config['dbuser']
            self.dbpass = config['dbpass']
            self.dbname = config['dbname']

    async def connect(self):
        self.db_pool = await aiomysql.create_pool(host=self.dbhost, port=3306,
                                                  user=self.dbuser, password=self.dbpass,
                                                  db=self.dbname, charset="utf8mb4",
                                                  loop=self.loop, autocommit=True)

    async def close(self):
        self.db_pool.close()
        await self.db_pool.wait_closed()

    async def check_angry(self, title):
        async with self.db_pool.acquire() as conn:
            async with conn.cursor() as cur:
                nowtime = int(time.time())
                rowcount = await cur.execute("SELECT * FROM angry_revert WHERE title=%s AND (expiry = 0 OR expiry >= %s)", (title, nowtime))
                return rowcount >= 1

    async def add_vandalism_entry(self, revid, title, user, patternlist, score, pattern=""):
        async with self.db_pool.acquire() as conn:
            async with conn.cursor() as cur:
                return await cur.execute("INSERT INTO vandalism_log VALUES (%s, NOW(), %s, %s, %s, %s, %s)",
                                        (revid, user, title, patternlist, pattern, score))

    async def add_good_entry(self, revid, title, user, patternlist, score, pattern=""):
        async with self.db_pool.acquire() as conn:
            async with conn.cursor() as cur:
                return await cur.execute("INSERT INTO good_log VALUES (%s, NOW(), %s, %s, %s, %s, %s)",
                                        (revid, user, title, patternlist, pattern, score))
