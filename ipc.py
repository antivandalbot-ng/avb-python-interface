import asyncio
import atexit
import base64
import hmac
import json
import logging
import socket


class ProtocolError(RuntimeError):
    def __init__(self, message):
        self.message = message


class IPCProtocol(asyncio.Protocol):
    def __init__(self, bot, loop):
        self.logger = logging.getLogger("antivandalbot.{}".format(__name__))
        self.bot = bot
        self.loop = loop
        self.closed = False

    def data_received(self, data):
        if data.decode("utf-8").startswith("#ERROR MSG AUTH"):
            raise ProtocolError("IPC message authentication failed")

    def connection_made(self, transport):
        self.logger.debug("IPC connection established.")

    def connection_lost(self, exc):
        if not self.closed:
            self.logger.error("IPC connection lost, re-establishing in 30 seconds...")
            self.loop.create_task(self.bot.ipc.connect(self.bot, delay=30))

    def close(self):
        self.closed = True


class IPC:
    def __init__(self, loop):
        self.logger = logging.getLogger("antivandalbot.{}".format(__name__))
        with open("config.json") as c:
            config = json.load(c)
            self.ipchost = config['ipchost']
            self.ipcport = config['ipcport']
            self._ipckey = config['ipckey'].encode("utf-8")
        self.loop = loop
        self.bot = None
        self.transport = None
        self.protocol = None
        atexit.register(self.cleanup)

    def cleanup(self):
        try:
            self.protocol.close()
            self.transport.close()
        except:
            pass

    async def connect(self, bot=None, delay=0):
        self.transport = None
        if not bot:
            self.bot = bot
        await asyncio.sleep(delay)
        while not self.transport:
            try:
                self.transport, self.protocol = await self.loop.create_connection(lambda: IPCProtocol(bot, self.loop), self.ipchost,
                                                                                  int(self.ipcport))
                self.transport.set_write_buffer_limits(0)
            except ConnectionRefusedError:
                self.logger.error("IPC connection refused, retrying in 30 seconds...")
                await asyncio.sleep(30)

    async def write(self, msg):
        if not self.transport:
            return
        encoded_msg = base64.b64encode(msg.encode("utf-8"))
        h = hmac.new(self._ipckey, msg=encoded_msg, digestmod="sha224")
        self.transport.write("{}::{}\n".format(encoded_msg.decode("utf-8"), h.hexdigest()).encode("utf-8"))

    async def send_irc_msg(self, channel, msg):
        if isinstance(channel, list):
            for c in channel:
                await self.write("PRIVMSG {} :{}".format(c, msg))
        else:
            await self.write("PRIVMSG {} :{}".format(channel, msg))
