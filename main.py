#!/usr/bin/env python3
import asyncio
import functools
import json
import logging
import pprint
import signal
import sys
import time
from datetime import datetime, timezone
from logging import handlers

import aiohttp
from aiosseclient import aiosseclient

import utils
from botcore import BotCore
from controldb import ControlDB
from ipc import IPC
from redisdb import RedisDB
from scoring import EditScorer
from wikiapi import WikiAPI

try:
    import uvloop
except ImportError:
    pass
else:
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())


def init_logging(config):
    logger = logging.getLogger("antivandalbot")
    logformat = "%(asctime)s.%(msecs)03d %(name)s (%(process)d) %(levelname)s:%(message)s"
    log_formatter = logging.Formatter(fmt=logformat, datefmt='%Y-%m-%d %H:%M:%S')
    stderr_handler = logging.StreamHandler()
    stderr_handler.setFormatter(log_formatter)
    stderr_handler.setLevel(logging.INFO)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(stderr_handler)
    if config.get("debuglog"):
        file_handler = handlers.RotatingFileHandler(config['debuglog'], maxBytes=3*1024*1024, backupCount=2, encoding="utf-8")
        file_handler.setFormatter(log_formatter)
        file_handler.setLevel(logging.DEBUG)
        logger.addHandler(file_handler)
    return logger


async def reload():
    global config
    logger.info("SIGHUP received, reloading config...")
    with open("config.json") as c:
        config = json.load(c)
    scorer.load()
    bot.reload()


async def sig_handler(loop, signal=None):
    try:
        if signal:
            logger.info("Caught signal {}".format(signal.name))
        logger.info("Exiting...")
        tasks = [task for task in asyncio.Task.all_tasks() if task is not
                 asyncio.tasks.Task.current_task()]
        list(map(lambda task: task.cancel(), tasks))
        results = await asyncio.gather(*tasks, return_exceptions=True)
        logger.info("Shutting down redis pool...")
        await redis.close()
        logger.info("Closing aiohttp sessions...")
        await session.close()
        logger.info("Closing database connections...")
        await controldb.close()
    finally:
        loop.stop()


async def process_rev(data):
    start = time.perf_counter()
    if data['type'] != "edit":
        return
    if data['bot']:
        return
    if data['user'] == config['wikiuser']:
        return
    if data['namespace'] in [-1, 2, 3, 8]:
        return
    if data['title'] in config['excluded']:
        return
    logger.debug("Processing revision {}...".format(data['revision']['new']))
    if not utils.is_ipaddress(data['user']):
        # Registered user
        user_info = await api.get_user_info(data['user'])
        if any([group in ["sysop", "rollbacker"] for group in user_info['groups']]) or user_info['editcount'] > config['editcount']:
            end = time.perf_counter()
            logger.debug("Took {:.4f} seconds.".format(end - start))
            return
    try:
        revscore = await loop.run_in_executor(None, functools.partial(scorer.score, data['revision']['new']))
        readable_score = "{}{:.5f}{}".format(chr(0x02) if revscore['probability']['vandalism'] >= config['detectlevel'] else "",
                                             revscore['probability']['vandalism'],
                                             chr(0x0F) if revscore['probability']['vandalism'] >= config['detectlevel'] else "")
        for channel in config['ownerchan']:
            await ipc.write("PRIVMSG {} :(AVB-2) {} edited {} ({}) Diff: {}{}/index.php?diff={}".format(
                channel, data['user'], data['title'], readable_score, data['server_url'],
                data['server_script_path'], data['revision']['new']
            ))
    except Exception as e:
        logger.exception(e)
        await ipc.send_irc_msg(config['ownerchan'], "(AVB-2) Error predicting diff {}".format(data['revision']['new']))
        end = time.perf_counter()
        logger.debug("Took {:.4f} seconds.".format(end - start))
        return
    if revscore['probability']['vandalism'] >= config['detectlevel']:
        # Add to SWMTBot's blacklist
        await ipc.send_irc_msg(config['cvnchannel'], "{} bl add {} x={} r=vandalism on [[{}:{}]]".format(config['alertbot'], data['user'],
                                                                                                         config['blacklisttime'], config['wikilang'],
                                                                                                         data['title']))
        last_revert, angry_rvt = await asyncio.gather(redis.get_last_revert_time(data['user'], data['title']),
                                                      controldb.check_angry(data['title']))
        last_time = (datetime.now(timezone.utc) - last_revert).total_seconds()
        if last_time > 24*60*60 or angry_rvt:
            asyncio.ensure_future(controldb.add_vandalism_entry(data['revision']['new'], data['title'], data['user'], "revscore",
                                  round(revscore['probability']['vandalism'], 6), pattern=scorer.model_type))
            rb_result = await api.rollback(data['title'], data['user'])
            if rb_result.get("error"):
                error_desc = {
                    "missingtitle": "Page has been deleted.",
                    "alreadyrolled": "Someone has done the rollback already.",
                    "onlyauthor": "User is the only author.",
                    "blocked": "I have been blocked!",
                    "autoblocked": "I have been autoblocked!"
                }
                try:
                    result_msg = error_desc[rb_result['error']['code']]
                except KeyError:
                    result_msg = "MediaWiki API returned: {}".format(rb_result['error']['code'])
            else:
                await redis.set_last_revert_time(data['user'], data['title'])
                await bot.warn(data['user'], data['title'], revscore['probability']['vandalism'],
                               data['revision']['new'])
                result_msg = "Reverted and user warned."
            await ipc.send_irc_msg(config['ownerchan'], "(AVB-2) [[{}:{}]]: {}".format(config['wikilang'], data['title'], result_msg))
        else:
            asyncio.ensure_future(controldb.add_vandalism_entry(data['revision']['new'], data['title'], data['user'], "revscore-skipped",
                                  round(revscore['probability']['vandalism'], 6), pattern=scorer.model_type))
            await ipc.send_irc_msg(config['ownerchan'], "(AVB-2) Not reverting the same user/page combination.")
            await ipc.send_irc_msg(config['cvnchannel'], "!alert Possible missed vandalism on [[{lang}:{}]] by [[{lang}:User:{}]]: {}{}/index.php?diff={}".format(
                data['title'], data['user'], data['server_url'], data['server_script_path'], data['revision']['new'], lang=config['wikilang']
            ))
    elif revscore['probability']['vandalism'] <= config.get("goodlevel", 0):
        await controldb.add_good_entry(data['revision']['new'], data['title'], data['user'], "revscore",
                                       round(revscore['probability']['vandalism'], 6), pattern=scorer.model_type)
    end = time.perf_counter()
    logger.debug("Took {:.4f} seconds.".format(end - start))
    return


async def main():
    while True:
        try:
            async for event in aiosseclient('https://stream.wikimedia.org/v2/stream/recentchange', timeout=None):
                try:
                    change = json.loads(event.data)
                    if change['wiki'] == config['wiki']:
                        await process_rev(change)
                except json.JSONDecodeError:
                    logger.error("JSON decode error: {}".format(pprint.pformat(event.data)))
                    continue
                except asyncio.CancelledError:
                    raise
                except:
                    logger.exception("Something went wrong:")
                    continue
        except asyncio.CancelledError:
            return
        except aiohttp.client_exceptions.ClientPayloadError:
            logger.exception("SSE error:")
            await asyncio.sleep(30)
        except:
            logger.exception("Something went wrong:")
            await asyncio.sleep(30)

if __name__ == "__main__":
    start = time.perf_counter()
    try:
        with open("config.json") as c:
            config = json.load(c)
    except FileNotFoundError:
        print("Config file not found, quitting!")
        sys.exit(-1)
    logger = init_logging(config)
    logger.info("Initializing...")
    loop = asyncio.get_event_loop()
    loop.add_signal_handler(signal.SIGTERM,
                            lambda: asyncio.ensure_future(sig_handler(loop, signal.SIGTERM)))
    loop.add_signal_handler(signal.SIGINT,
                            lambda: asyncio.ensure_future(sig_handler(loop, signal.SIGINT)))
    loop.add_signal_handler(signal.SIGHUP,
                            lambda: asyncio.ensure_future(reload()))
    session = aiohttp.ClientSession(loop=loop, headers={"User-Agent": "User:{} ({})".format(config['wikiuser'], config['wiki'])})
    api = WikiAPI(config['wikiurl'], session)
    scorer = EditScorer(config['wikiurl'], user_agent="User:{} ({})".format(config['wikiuser'], config['wiki']))
    ipc = IPC(loop)
    controldb = ControlDB(loop)
    redis = RedisDB()
    bot = BotCore(api, ipc)
    loop.create_task(ipc.connect(bot))
    loop.create_task(controldb.connect())
    loop.create_task(main())
    loop.create_task(bot.check_blocks())
    end = time.perf_counter()
    logger.debug("Initialization completed in {:.4f} seconds.".format(end - start))
    try:
        loop.run_forever()
    finally:
        loop.close()
